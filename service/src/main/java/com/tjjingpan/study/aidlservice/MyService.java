package com.tjjingpan.study.aidlservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import androidx.annotation.Nullable;

public class MyService extends Service {
    public MyService() {
    }

    @Override
    public void onCreate() {
        System.out.println("Service started");
        //启动一个线程每1秒在输出data数据，观察数据变化
        new Thread(){
            @Override
            public void run() {
                super.run();
                running = true;
                while(running){
                    try {
                        System.out.println(data);
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }.start();

    }

    @Override
    public void onDestroy() {
        System.out.println("Service destory");
        running = false;
    }

    private String data="默认数据";
    private boolean running = false;//标记线程是否在运行
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new  IServiceRemoteBindler.Stub(){
            @Override
            public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString) throws RemoteException {

            }

            @Override
            public void setData(String data) throws RemoteException {
                MyService.this.data = data; //修改默认数据
            }
        };
    }


}